import React, { Component } from "React";

class Proyecto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: {
                isSelect: false,
                card_proyect: "card card-proyect",
                card_body: "card-body"
            }
        };
        this.toggleProyect = this.toggleProyect.bind(this);
    }

    componentDidMount() {
        this.setState({
            selected: {
                isSelect: false,
                card_proyect: "card card-proyect",
                card_body: "card-body"
            }
        });
    }

    toggleProyect() {
        if(this.state.selected.isSelect) {
            this.setState({
                selected: {
                    isSelect: false,
                    card_proyect: "card card-proyect",
                    card_body: "card-body"
                }
            });
        }else {
            this.setState({
                selected: {
                    isSelect: true,
                    card_proyect: "card card-proyect select",
                    card_body: "card-body select"
                }
            });
        }
    }

    render() {
        return (
            <div className={this.state.selected.card_proyect} onClick={this.toggleProyect}>
                <div className={this.state.selected.card_body}>
                    <span className="badge badge-primary">Toptal</span>
                    <h5 className="card-title">Duito App Desing</h5>
                    <h6 className="card-subtitle">Mobil app design for fiance wallet app</h6>
                    <div className="perfiles d-flex justify-content-end">
                        <div
                            className="foto"
                            style={{ backgroundImage: "url(" + "images/perfil2.jpg" + ")" }}
                        ></div>
                        <div
                            className="foto"
                            style={{ backgroundImage: "url(" + "images/perfil3.jpg" + ")" }}
                        ></div>
                        <div className="icons">
                            <span>
                                <i className="fa fa-check-square"></i>2/4
                            </span>
                            <span>
                                <i className="fa fa-paperclip"></i>4
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Proyecto;
