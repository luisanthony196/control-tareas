import React, { Component } from "React";

import Proyecto from "./proyecto";
import AreaTrabajo from "./areatrabajo";
import Usuario from "./usuario";
import MenuItem from "./menuitem";

class Principal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hide: {
                isHide: false,
                d_flex: "d-flex",
                item: "item"
            }
        };
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    componentDidMount() {
        this.setState({
            hide: {
                isHide: false,
                d_flex: "d-flex",
                item: "item"
            }
        });
    }

    toggleMenu() {
        if(this.state.hide.isHide) {
            this.setState({
                hide: {
                    isHide: false,
                    d_flex: "d-flex",
                    item: "item"
                }
            });
        }else {
            this.setState({
                hide: {
                    isHide: true,
                    d_flex: "d-flex toggled",
                    item: "item borrado"
                }
            });
        }
    }

    render() {
        return (
            <div className={this.state.hide.d_flex} id="wrapper">
                    {/* Columna con las vistas */}
                    <div className="bg-white border-right" id="sidebar-wrapper">
                        <div className="sidebar-heading border-bottom">
                            <i className="fa fa-bars" id="menu-toggle" onClick={this.toggleMenu}></i>
                            <h4 className={this.state.hide.item}>TASKING</h4>
                        </div>
                        <div className="list-group list-group-flush justify-content-center">
                            <MenuItem name="USUARIOS" icon="fa-user" hide={this.state.hide.item}/>
                            <MenuItem name="PROYECTOS" icon="fa-folder-open" hide={this.state.hide.item}/>
                            <MenuItem name="AJUSTES" icon="fa-cog" hide={this.state.hide.item}/>
                        </div>
                    </div>
                    {/* Columna con los proyectos */}
                    <div className="bg-white border-right" id="proyect-wrapper">
                        <div className="proyect-heading border-bottom">
                            <h4>Project Board</h4>
                            <button className="btn btn-primary" type="button">
                                <i className="fa fa-plus"></i>
                            </button>
                        </div>
                        <div
                            className="accordion md-accordion"
                            id="accordionEx"
                            role="tablist"
                            aria-multiselectable="true"
                        >
                            <Proyecto />
                            <Proyecto />
                            <Proyecto />
                            <Proyecto />
                            <Proyecto />
                        </div>
                    </div>
                    {/* Columna con la barra, el perfil y el contenido */}
                    <div id="page-content-wrapper">
                        <div className="container-fluid page-content-header border-bottom">
                            <nav className="navbar navbar-expand-lg navbar-light bg-white justify-content-between">
                                <div className="search">
                                    <input
                                        className="form-control"
                                        type="text"
                                        placeholder="Search"
                                        aria-label="Search"
                                    />
                                </div>
                                <div className="icon">
                                    <i className="fa fa-code-branch"></i>
                                    <i className="fa fa-bell"></i>
                                </div>
                            </nav>
                            <Usuario token={this.props.token}/>
                        </div>
                        <AreaTrabajo />
                    </div>
                </div>
            );
    }
}
export default Principal;