const express = require('express');
const router = express.Router();

const Feedback = require('../models/feedback')

router.get('/', async (req, res) => {
    const feedbacks = await Feedback.find();
    res.json(feedbacks);
});

router.get('/:id', async (req, res) => {
    const feedback = await Feedback.findById(req.params.id);
    res.json(feedback);
});

router.post('/', async (req, res) =>{
    const {comentario, adjunto, empresa} = req.body;
    const feedback = new Feedback({comentario, adjunto, empresa});
    await feedback.save(),
    res.json({status: 'Feedback almacenado'});
});

router.put('/:id', async (req, res) =>{
    const {comentario, adjunto, empresa} =req.body;
    const newFeedback = {comentario, adjunto, empresa};
    await Feedback.findByIdAndUpdate(req.params.id, newFeedback);
    console.log(req.params.id);
    res.json({status: 'Feedback actualizado'});
});

router.delete('/:id', async (req,res) => {
    await Feedback.findByIdAndRemove(req.params.id);
    res.json({status: 'Feedback eliminado'});
});

module.exports = router;