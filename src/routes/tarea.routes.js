const express = require('express');
const router = express.Router();

const Tarea = require('../models/tarea')

router.get('/', async (req, res) => {
    const tareas = await Tarea.find();
    res.json(tareas);
});

router.get('/:id', async (req, res) => {
    const tarea = await Tarea.findById(req.params.id);
    res.json(tarea);
});

router.post("/", async (req, res) => {
    const { nombre, tipo, prioridad, f_inicio, f_entrega, usuarios, comentarios } = req.body;
    const tarea = new Tarea({
        nombre,
        tipo,
        prioridad,
        f_inicio,
        f_entrega,
        usuarios,
        comentarios
    });
    await tarea.save(), res.json({ status: "Tarea almacenada" });
});

router.put("/:id", async (req, res) => {
    const { nombre, tipo, prioridad, f_inicio, f_entrega, usuarios, comentarios } = req.body;
    const newTarea = { nombre, tipo, prioridad, f_inicio, f_entrega, usuarios, comentarios };
    await Tarea.findByIdAndUpdate(req.params.id, newTarea);
    console.log(req.params.id);
    res.json({ status: "Tarea actualizada" });
});

router.delete("/:id", async (req, res) => {
    await Tarea.findByIdAndRemove(req.params.id);
    res.json({ status: "Tarea eliminada" });
});

module.exports = router;
