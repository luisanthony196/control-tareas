const mongoose = require('mongoose');
const {Schema} = mongoose;

const FeedbackSchema = new Schema({
    comentario: {type: String, required: true},
    adjunto: {type: String, required: true},
    empresa: {
        type: Schema.Types.ObjectId,
        ref: 'empresa'
    }
},{timestamps: true});

module.exports = mongoose.model('Feedback',FeedbackSchema);