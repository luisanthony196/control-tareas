const mongoose = require('mongoose');
const {Schema} = mongoose;

const ProyectoSchema = new Schema({
    nombre: {type: String, required: true},
    descripcion: {type: String, required: true},
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'usuario'
    },
    tareas: [{
        type: Schema.Types.ObjectId,
        ref: 'tarea'
    }]
},{timestamps: true});

module.exports = mongoose.model('Proyecto', ProyectoSchema);