const mongoose = require('mongoose');
const {Schema} = mongoose;

const UsuarioSchema = new Schema({
    email: {type: String, required: true},
    password: {type: String},
    nombres: {type: String, required: true},
    perfil: {type: String, required: true},
    imagen: {type: String}
},{timestamps: true});

module.exports = mongoose.model('Usuario', UsuarioSchema);