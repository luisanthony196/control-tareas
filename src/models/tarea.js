const mongoose = require('mongoose');
const {Schema} = mongoose;

const TareaSchema = new Schema({
    nombre: {type: String, required: true},
    tipo: {type: String, required: true},
    prioridad: {type: String, required: true},
    f_inicio: {type: Date, required: true},
    f_entrega: {type: Date, required: true},
    usuarios: [{
        type: Schema.Types.ObjectId,
        ref: 'usuario'
    }],
    comentarios: [{
        type: Schema.Types.ObjectId,
        ref: 'actividad'
    }],
},{timestamps: true});

module.exports = mongoose.model('Tarea', TareaSchema);