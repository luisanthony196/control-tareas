const mongoose = require('mongoose');

const URI = 'mongodb://localhost/control-tareas';

mongoose.connect(URI, 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
.then(db =>console.log('DB conectada'))
.catch(err => console.log(err));

module.exports = mongoose;