const express = require('express');
const morgan = require('morgan');
const path = require('path');

const { mongoose } = require('./database');

const app = express();

//Settings
app.set('port', process.env.PORT||3000);

//Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Routes
app.use('/api/usuario', require('./routes/usuario.routes'));
app.use('/api/tarea', require('./routes/tarea.routes'));
app.use('/api/proyecto', require('./routes/proyecto.routes'));
app.use('/api/comentario', require('./routes/comentario.routes'));

//Static files
app.use(express.static(path.join(__dirname, 'public')));

//Starting the server
app.listen(app.get('port'), () =>{
    console.log(`server on port ${app.get('port')}`)
});